# Custom Menu plugin

![screenshot](https://gitlab.com/francoisjacquet/Custom_Menu/raw/main/screenshot.png?inline=false)

https://gitlab.com/francoisjacquet/Custom_Menu/

Version 1.1 - March, 2025

Author François Jacquet

Sponsored by AT group, Slovenia

License GNU/GPLv2 or later

## Description

RosarioSIS plugin to customize the left menu. Reorder menu entries: drag and drop the modules in the list and save their new order.

Translated in French and Spanish.

Note: once saved, the custom menu order is kept even if the plugin is deactivated.


## Content

Plugin Configuration
- Order Modules

## Install

Copy the `Custom_Menu/` folder (if named `Custom_Menu-main`, rename it) and its content inside the `plugins/` folder of RosarioSIS.

Go to _School > Configuration > Plugins_ and click "Activate".

Requires RosarioSIS 11.6+
